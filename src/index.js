'use strict';

const update = require('immutability-helper');

// Clone the nav tree in each page's metadata. The cloned tree has
// the current page annotated as "active" so the node can be
// highlighted in menus etc.
module.exports = () => {
	return (files, metalsmith, done) => {
		for (const filename in files) {
			if (files.hasOwnProperty(filename)) {
				const navs = metalsmith.metadata().navs;
				const file = files[filename];

				const annotateNav = (nav) => {
					let isActive = false;
					let isFile = false;
					let isDirectory = false;
					let activeChild = false;
					let children = [];

					if (nav.file) {
						const itempath = nav.file.nav_path;
						const filepath = file.nav_path;

						isFile = true;

						if (itempath === filepath) {
							isActive = true;
						}
					} else {
						isDirectory = true;
					}

					if (nav.children) {
						children = nav.children.map((child) => {
							const annotatedChild = annotateNav(child);
							activeChild = activeChild || annotatedChild.active || annotatedChild.activeChild;
							return annotatedChild;
						});
					}

					const changes = {
						active: {
							$set: isActive
						},
						activeChild: {
							$set: activeChild
						},
						children: {
							$set: children
						},
						isFile: {
							$set: isFile
						},
						isDirectory: {
							$set: isDirectory
						}
					};

					return update(nav, changes);
				};

				const changes = {};

				for (const group in navs) {
					if (navs.hasOwnProperty(group)) {
						changes[group] = {
							$set: navs[group].map(annotateNav)
						};
					}
				}

				file.navs = update(navs, changes);
			}
		}

		done();
	};
};
